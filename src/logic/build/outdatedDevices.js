import releasesData from "@data/releases.json";

import slugify from "@sindresorhus/slugify";

function supersededByDefaultRelease(release) {
  if (release == releasesData.default) return false;

  do {
    release = releasesData.list.find((r) => r.name == release)?.supersededBy;
  } while (!!release && release != releasesData.default);

  return release == releasesData.default;
}

export default function getOutdatedDevices(allDevices) {
  return allDevices
    .filter(
      (d, i, s) =>
        !d.releases.default &&
        d.releases.all.some((r) => supersededByDefaultRelease(r.name)) &&
        i == s.findIndex((dev) => dev.codename == d.codename)
    )
    .map((d) => {
      const defaultRelease = releasesData.list.find(
        (r) => r.name == releasesData.default
      );
      return {
        codename: d.codename,
        name: d.name,
        path: "/device/" + slugify(d.codename, { decamelize: false }),
        releases: {
          current: {
            path: "/device/" + slugify(d.codename, { decamelize: false }),
            deviceParam: slugify(d.codename, { decamelize: false }),
            ...defaultRelease
          },
          suggested: null,
          default: null,
          all: JSON.parse(JSON.stringify(d.releases.all))
        }
      };
    });
}
