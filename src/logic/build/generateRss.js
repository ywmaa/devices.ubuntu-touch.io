// Import libraries
import { Feed } from "feed";
import pMemoize from "p-memoize";

// Import data
import getDevices from "./getDevices.js";

async function feedGenerator(memoCode) {
  const feed = new Feed({
    id: "devices.ubuntu-touch.io/new",
    title: "Ubuntu Touch • Latest devices",
    description:
      "Didn't found your device in the list? Subscribe to this feed and you'll know when a new Ubuntu Touch device gets ported.",
    link: "https://devices.ubuntu-touch.io/",
    language: "en",
    image: "https://devices.ubuntu-touch.io/social-preview.jpg",
    favicon: "https://devices.ubuntu-touch.io/favicon.ico",
    copyright:
      "© Copyright " + new Date().getFullYear() + " UBports Foundation",
    feedLinks: {
      rss: "https://devices.ubuntu-touch.io/new.xml",
      atom: "https://devices.ubuntu-touch.io/new.atom"
    },
    author: {
      name: "UBports contributors",
      link: "https://ubports.com/"
    }
  });

  const devices = await getDevices("get-all-devices");

  devices.forEach((node) => {
    if (!node.variantOf) feed.addItem(generateNode(node));
  });

  return feed;
}

function generateNode(node) {
  let firstCommit = node.gitData[node.gitData.length - 1];
  return {
    id: node.codename,
    title: node.name,
    date: firstCommit.authorDate,
    description: "New device added to the devices website, check it out.",
    content:
      "<p>" +
      node.name +
      " was just added to the devices page.</p>" +
      (node.description ? "<p>" + node.description + "</p>" : "") +
      "<p>" +
      node.progressStage.description +
      "</p>" +
      "<p>Added by " +
      firstCommit.authorName +
      "</p>",
    image: "https://devices.ubuntu-touch.io/social-preview.jpg",
    link: "https://devices.ubuntu-touch.io" + node.path,
    author: {
      name: "UBports contributors",
      link: "https://ubports.com/"
    }
  };
}

export default pMemoize(feedGenerator);
