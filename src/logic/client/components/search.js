import List from "list.js";

// Regenerate search results
function filterDevices(listManager) {
  listManager.filter((device) => applyFilters(device));
  const sortOrder = Object.values(
    document.querySelectorAll("[name='sortOrder']")
  );
  const sortBy = sortOrder.filter((e) => e.checked)[0];
  listManager.sort(sortBy.value, {
    order: sortBy.parentElement.childNodes[1].dataset.order || "asc"
  });
}

// Apply search filters
function applyFilters(device) {
  const filters = getFilterValues([
    "deviceType",
    "portType",
    "featureStage",
    "installerFilter",
    "waydroidFilter",
    "displayOutFilter",
    "release"
  ]);
  let d = device.values();
  return (
    (!filters[0].length || filters[0].includes(d.deviceType)) &&
    d.progressStage >= parseInt(filters[2]) &&
    (!filters[1].length || filters[1].includes(d.portType)) &&
    (filters[3] ? d.installerAvailable : true) &&
    (filters[4] ? d.waydroidAvailable : true) &&
    (filters[5] ? d.displayOutAvailable : true) &&
    filters[6] == d.release
  );
}

// Get filter values
function getFilterValues(query) {
  return query.map((elName) => {
    let elArray = Object.values(
      document.querySelectorAll("[name='" + elName + "']")
    );
    let filterVal = elArray.filter((e) => e.checked).map((e) => e.value);
    filterVal =
      elArray[0].type == "radio"
        ? filterVal[0]
        : elArray.length == 1
          ? filterVal.length == 1
          : filterVal;
    return filterVal;
  });
}

function disableFilters() {
  [
    "deviceType",
    "portType",
    "installerFilter",
    "waydroidFilter",
    "displayOutFilter"
  ].forEach((elName) => {
    document
      .querySelectorAll("[name='" + elName + "']")
      .forEach((el) => (el.checked = false));
  });
  let featureStageSelector = document.querySelector(
    "[name='featureStage'][value='0']"
  );
  featureStageSelector.checked = true;
  featureStageSelector.dispatchEvent(new Event("change"));
  _paq.push([
    "trackEvent",
    "Search filters",
    document.getElementById("disableFilters").dataset.trackingname,
    window.location.pathname
  ]);
}

// Set search results count in UI
function setResultsCount(count, unfilteredCount) {
  let deviceSearchCount = document.getElementById("deviceSearchCount");
  let searchNoResults = document.getElementById("searchNoResults");
  let filteredWarning = document.getElementById("filteredWarning");
  let deviceSearchMobileCount = document.getElementById("deviceCountMobile");

  if (unfilteredCount == 0) {
    searchNoResults.classList.remove("d-none");
    filteredWarning.classList.add("d-none");
  } else if (unfilteredCount > count) {
    searchNoResults.classList.add("d-none");
    filteredWarning.classList.remove("d-none");
  } else {
    searchNoResults.classList.add("d-none");
    filteredWarning.classList.add("d-none");
  }

  deviceSearchCount.innerText =
    count == 1 ? "One device" : count == 0 ? "No devices" : count + " devices";
  deviceSearchMobileCount.innerText =
    count == 1
      ? "One supported device"
      : count == 0
        ? "No supported devices"
        : count + " supported devices";
}

// Count number of devices when filters are disabled
function getUnfilteredCount(listManager) {
  const releaseFilter = getFilterValues(["release"]);
  return listManager.items.filter(
    (d) =>
      (listManager.searched ? d.found : true) &&
      d.values().release == releaseFilter[0]
  ).length;
}

// Decorate a function to execute only once stopped typing
function delayedCallback(cb) {
  let time;
  return (...args) => {
    clearTimeout(time);
    time = setTimeout(() => cb(...args), 1000);
  };
}

// Filters and search field to space separated string
function encodeKeywordAndFilters() {
  const filters = getFilterValues([
    "deviceType",
    "portType",
    "featureStage",
    "installerFilter",
    "waydroidFilter",
    "displayOutFilter",
    "release"
  ]);
  const keyword = document.querySelector("#searchInput").value;
  return [
    keyword,
    ...filters[0].map((e) => "type:" + e),
    ...filters[1].map((e) => "port:" + e),
    "compatStage:" + filters[2],
    "installer:" + filters[3],
    "waydroid:" + filters[4],
    "displayOut:" + filters[5],
    "release:" + filters[6]
  ]
    .join(" ")
    .trim();
}

// Mark current device link in bold
function markDeviceLink() {
  document.querySelectorAll(".active--exact").forEach((e) => {
    e.classList.remove("active--exact");
  });
  document
    .querySelector(
      "#sidebarCollapse [href='" +
        window.location.pathname.replace(/\/+$/, "") +
        "']"
    )
    ?.classList.add("active--exact");
}

const mtmSearch = delayedCallback((listManager) => {
  _paq.push([
    "trackSiteSearch",
    encodeKeywordAndFilters(),
    "Devices",
    listManager.matchingItems.length
  ]);
});

class SearchSidebar extends HTMLElement {
  constructor() {
    super();

    // Initialize list.js
    this._listmgr = new List("sidebarCollapse", {
      valueNames: [
        {
          data: [
            "codename",
            "release",
            "name",
            "progress",
            "price",
            "deviceType",
            "portType",
            "progressStage",
            "installerAvailable",
            "waydroidAvailable",
            "displayOutAvailable"
          ]
        }
      ],
      listClass: "devices-list",
      searchColumns: ["codename", "name"]
    });

    this._listmgr.on("filterComplete", (e) => {
      setResultsCount(
        this._listmgr.matchingItems.length,
        getUnfilteredCount(this._listmgr)
      );
      mtmSearch(this._listmgr);
    });

    this._listmgr.on("searchComplete", (e) => {
      setResultsCount(
        this._listmgr.matchingItems.length,
        getUnfilteredCount(this._listmgr)
      );
      mtmSearch(this._listmgr);
    });

    filterDevices(this._listmgr);
    markDeviceLink();

    // Register events
    this.querySelectorAll("input[name]").forEach((el) => {
      if (el.name != "sortOrder")
        el.onchange = () => {
          filterDevices(this._listmgr);
        };
    });
    this.querySelector("#disableFilters").onclick = disableFilters;

    document.addEventListener("astro:page-load", () => {
      // Hide sidebar on page load on mobile
      document.querySelector("#sidebarCollapse")?.classList.remove("show");
      markDeviceLink();
    });
  }
}

export default SearchSidebar;
