class HeaderContainer extends HTMLElement {
  constructor() {
    super();

    this.onclick = (e) => {
      let headerNav = document.getElementById("headerNav");
      if (!e.target.closest(".header") && headerNav) {
        headerNav.classList.remove("show");
        this.classList.remove("backdrop");
      }
    };
  }
}

export default HeaderContainer;
