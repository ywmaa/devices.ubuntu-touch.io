import slugify from "@sindresorhus/slugify";

import { navigate } from "astro:transitions/client";
import { startTracking, spaNavigationTrack } from "@logic/client/mtm-loader.js";

import DonateBanner from "@logic/client/components/donateBanner.js";
import DarkMode, {
  setDefaultColorMode
} from "@logic/client/components/darkMode.js";

import HeaderContainer from "@logic/client/components/header.js";
import ToggleButton from "@logic/client/components/toggleButton.js";
import SidenavScrollspy from "@logic/client/components/sidenav.js";
import SearchSidebar from "@logic/client/components/search.js";

import TippyTooltip from "@logic/client/components/tooltips.js";
import SidebarJS from "@logic/client/components/sidebars.js";
import StarSelector from "@logic/client/components/starSelector.js";

export default function registerComponents() {
  // Navigation
  let correctedPageSlug = window.location.pathname
    .split("/")
    .map((folder) => slugify(folder, { decamelize: false }))
    .join("/");
  if (correctedPageSlug != window.location.pathname)
    navigate(correctedPageSlug, { history: "replace" });

  // SPA transitions
  document.addEventListener("astro:before-swap", (ev) => {
    setDefaultColorMode(ev.newDocument);
  });

  // Components
  customElements.define("donate-banner", DonateBanner);
  customElements.define("dark-mode-switch", DarkMode);

  customElements.define("header-container", HeaderContainer);
  customElements.define("toggle-button", ToggleButton);
  customElements.define("sidenav-scrollspy", SidenavScrollspy);
  customElements.define("search-sidebar", SearchSidebar);

  customElements.define("sidebarjs-component", SidebarJS);
  customElements.define("tippy-tooltip", TippyTooltip);
  customElements.define("star-selector", StarSelector);

  // Tracking code
  document.addEventListener(
    "astro:page-load",
    () => {
      startTracking("https://analytics.ubports.com/", "3");
      document.addEventListener("astro:page-load", spaNavigationTrack);
    },
    { once: true }
  );
}
