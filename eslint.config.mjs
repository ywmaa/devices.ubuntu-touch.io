import prettierConfig from "eslint-plugin-prettier/recommended";
import astroPlugin from "eslint-plugin-astro";
import mdPlugin from "eslint-plugin-md";
import mdParser from "markdown-eslint-parser";

export default [
  {
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "module"
    }
  },
  ...astroPlugin.configs.recommended,
  prettierConfig,

  {
    files: ["**/*.md"],
    plugins: {
      md: mdPlugin
    },
    languageOptions: {
      parser: mdParser
    },
    rules: {
      "prettier/prettier": ["error", { parser: "markdown" }],
      "md/remark": "off"
    }
  },
  {
    files: ["**/*.astro"],
    rules: {
      "astro/no-unused-css-selector": "error",
      "astro/prefer-class-list-directive": "error",
      "astro/prefer-object-class-list": "error",
      "astro/prefer-split-class-list": "error"
    }
  }
];
