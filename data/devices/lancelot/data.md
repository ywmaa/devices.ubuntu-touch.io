---
name: "Xiaomi Redmi 9 and 9 Prime"
deviceType: "phone"
image: "https://fdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-9-2.jpg"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Mediatek Helio G80"
  - id: "gpu"
    value: "Mali-G52 MC2"
  - id: "rom"
    value: "32GB/64GB/128GB"
  - id: "ram"
    value: "3GB/4GB/6GB"
  - id: "android"
    value: "Android 10 Miui 11"
  - id: "battery"
    value: "5000 mAh"
  - id: "display"
    value: "IPS LCD, 400 nits (typ) 6.53 inches 1080 x 2340 pixels, 19.5:9 ratio (~395 ppi density)"
  - id: "arch"
    value: "arm64"
  - id: "rearCamera"
    value: "13MP(wide), 8MP(ultrawide), 5MP(macro), 2MP(depth)"
  - id: "frontCamera"
    value: "8MP"
  - id: "dimensions"
    value: "163.3 x 77 x 9.1 mm (6.43 x 3.03 x 0.36 in)"
  - id: "weight"
    value: "198 g (6.98 oz)"
contributors:
  - name: TheKit
    forum: "https://forums.ubports.com/user/thekit"
  - name: areyoudeveloper
    forum: "https://github.com/areyoudeveloper"
sources:
  portType: "community"
  portPath: "android10"
  deviceGroup: "xiaomi-redmi-9"
  deviceSource: "xiaomi-lancelot"
  kernelSource: "kernel-xiaomi-mt6768"
communityHelp:
  - name: "Device Support"
    link: "https://t.me/utlance"
---
